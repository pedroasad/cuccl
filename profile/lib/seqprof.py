# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2017 Pedro de Souza Asad
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/psa-exe/cuccl/>.
import profile as prf
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

DRIVER_BINARY = 'build/cuccl-seqprof'
NVPROF_BINARY = '/usr/local/cuda/bin/nvprof'

if __name__ == '__main__':
	repeat = 10
	resolutions = np.arange(1024, 8193, 256)
	times = np.zeros_like(resolutions, dtype=np.float)

	for i, resol in enumerate(resolutions):
		driverPart = '%s %d %d %d' % (DRIVER_BINARY, resol, resol, repeat)
		nvprofPart = '%s -u ms --print-gpu-summary --csv %s' % (NVPROF_BINARY, driverPart)
		filterPart = '%s 2>&1 | tail -n +6 | cut -d, -f4' % nvprofPart
		output = os.popen(filterPart).read()
		times[i] = sum(float(x) for x in output.split('\n') if x)

		sys.stdout.write('Progress: %5.1f%%\r' % (100.0 * (i + 1) / len(resolutions)))
		sys.stdout.flush()

	fig = prf.figure(1.0, 'single')

	plt.plot(resolutions, times)
	plt.xticks(resolutions[::4])
	plt.xlabel('Resolution')
	plt.ylabel('Processing time (ms)')
	plt.grid()

	prf.savefig(fig, 'profile/out/plots/seqprof')
