# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2017 Pedro de Souza Asad
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/psa-exe/cuccl/>.

import copy
import itertools as it
import numpy as np
import profile as prf
import math
import matplotlib.pyplot as plt
import profile.image as profile
import sys

from profile import args, colors, ylim
from profile.shapes import multiridge_n

# =============================================================================
# Single Ridge Pattern with increasing level
# =============================================================================
print 'Profiling %s' % __file__
combinations = list(it.product(args.resolution, args.algorithm, args.properties, args.level))

params = {}
stats  = {}

for i, combo in enumerate(combinations):
	r, a, p, l = combo

	subdivisions = int(np.log(r) / np.log(2)) - 7 - 1
	# subdivisions = min(5, subdivisions)
	step = 4**subdivisions / 16

	params[combo] = {
		'additionalparams': ' '.join(['-p%s' % p, args.sequential]),
		'algorithm'       : a,
		'filepattern'     : 'instancing-resol:%d-alg:%s-props:%s%s-level:%d-subdiv:%d-inst:%%d' % (r, a, p, args.sequential, l, subdivisions),
		'paramrange'      : np.arange(step, 4**subdivisions+1, step),
		'repeat'          : 100,
		'resolution'      : r, 
		'shapefunc'       : multiridge_n(l, subdivisions),
		'withmemory'      : True,
	}

	print '  %2d/%2d: %r' % (i+1, len(combinations), combo)
	if not profile.checkFiles(**params[combo]):
		profile.profile(**params[combo])

	stats[combo] = profile.stats(**params[combo])

for res in args.resolution:
	cols = len(args.algorithm) * len(args.properties)
	rows = len(args.level)

	figure, axes = prf.subplots(rows, cols, mode='double', aspect=1.0, sharex=True, sharey=True)
	axes = np.array(axes, dtype=np.object).reshape((rows, cols))

	for i, lv in enumerate(args.level):
		for j, (props, alg) in enumerate(it.product(args.properties, args.algorithm)):
			key = (res, alg, props, lv)

			xvalues = params[key]['paramrange']

			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'R'       ) / 1000,                                            color=colors['R'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RI'      ) / 1000, profile.aggr(stats[key], 'R'     ) / 1000, color=colors['I'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIj'     ) / 1000, profile.aggr(stats[key], 'RI'    ) / 1000, color=colors['j'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJ'    ) / 1000, profile.aggr(stats[key], 'RIj'   ) / 1000, color=colors['J'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJf'   ) / 1000, profile.aggr(stats[key], 'RIjJ'  ) / 1000, color=colors['f'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfF'  ) / 1000, profile.aggr(stats[key], 'RIjJf' ) / 1000, color=colors['F'])
			axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, profile.aggr(stats[key], 'RIjJfF') / 1000, color=colors['M'])

			axes[i][j].plot(xvalues, profile.aggr(stats[res, alg, 'late', lv], 'RIjJfF' ) / 1000, 'k--')

			if props == 'late':
			 	axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMP' ) / 1000, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, color=colors['P'])
			# 	axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMPO') / 1000, profile.aggr(stats[key], 'RIjJfFMP') / 1000, color=colors['O'])
			# else:
			# 	axes[i][j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMO' ) / 1000, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, color=colors['O'])

			axes[i][j].set_ylim(*ylim[res])
			axes[i][j].grid()

			xstep = xvalues[1] - xvalues[0]
			xvalues = np.hstack(([0], xvalues))
			axes[i][j].set_xticks(xvalues[::2])
			axes[i][j].set_xticklabels(['%d' % (x / xstep) for x in xvalues[::2]])

			# if props == 'yes':
			# 	axes[0][j].set_title('Early %s' % alg.upper(), fontsize=10)
			# elif props == 'no':
			# 	axes[0][j].set_title('%s' % alg.upper(), fontsize=10)
			# elif props == 'late':
			# 	axes[0][j].set_title('Late %s' % alg.upper(), fontsize=10)

		axes[i][0].set_ylabel('Performance (ms)')

	# axes[-1][0].set_xlabel(r'Instances ($\times 2^{%d}$)' % math.log(xstep, 2))

	prf.savefig(
		figure,
		'profile/out/plots/instancing%s-resol:%d' % (args.sequential, res),
	)
	plt.close(figure)

	figure, axes = prf.subplots(1, 1, mode='double', aspect=0.12, frameon=False)
	axes.plot([], [], 'k--', label='Ordinary time')
	axes.fill_between([], [], color=colors['P'], label='Properties Sum (Late only)')
	axes.fill_between([], [], color=colors['M'], label='Memset (not in Early LE)')
	axes.fill_between([], [], color=colors['J'], label='Later Scan/Global Merge')
	axes.fill_between([], [], color=colors['j'], label='First Scan/Global Merge')
	axes.fill_between([], [], color=colors['I'], label='Prelabel/Local Merge')
	if args.sequential:
		axes.fill_between([], [], color=colors['R'], label='Sequential Relabeling')
	else:
		axes.fill_between([], [], color=(0,0,0,0)  , label='  ')
	axes.fill_between([], [], color=colors['F'], label='Later Analysis/Path Compression')
	axes.fill_between([], [], color=colors['f'], label='First Analysis/Path Compression')
	axes.xaxis.set_visible(False)
	axes.yaxis.set_visible(False)
	axes.axis('off')
	legend = axes.legend(loc='upper left', ncol=3)

	prf.savefig(
		figure,
		'profile/out/plots/legend%s' % args.sequential,
		legend,
	)
	plt.close(figure)

plt.show()

linefmt = '    %20r | %4s : %4s | %4.1f | %4.1f | %4.1f'
headfmt = '    %20s | %4s : %4s | %4s | %4s | %4s'
header = headfmt % ('Parameters', 'p1', 'p0', 'min', 'max', 'avg')

print '  Resolution ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.algorithm, args.properties, args.level))
for combo in combinations:
	a, p, l = combo
	r0 = args.resolution[0]
	for r1 in args.resolution[1:]:
		y0 = profile.aggr(stats[r0, a, p, l], 'OIjJFMP')
		y1 = profile.aggr(stats[r1, a, p, l], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, r1, r0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

print '  Algorithm ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.resolution, args.properties, args.level))
for combo in combinations:
	r, p, l = combo
	a0 = args.algorithm[0]
	for a1 in args.algorithm[1:]:
		y0 = profile.aggr(stats[r, a0, p, l], 'OIjJFMP')
		y1 = profile.aggr(stats[r, a1, p, l], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, a1, a0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

print '  Propsmodes ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.resolution, args.algorithm, args.level))
for combo in combinations:
	r, a, l = combo
	p0 = args.properties[0]
	for p1 in args.properties[1:]:
		y0 = profile.aggr(stats[r, a, p0, l], 'OIjJFMP')
		y1 = profile.aggr(stats[r, a, p1, l], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, p1, p0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))
