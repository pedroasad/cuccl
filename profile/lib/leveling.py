# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2017 Pedro de Souza Asad
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/psa-exe/cuccl/>.

import copy
import code
import itertools as it
import profile as prf
import matplotlib.pyplot as plt
import numpy as np
import profile.image as profile
import sys

from profile import args, colors, ylim
from profile.shapes import ridge

# =============================================================================
# Single Ridge Pattern with increasing level
# =============================================================================
print 'Profiling %s' % __file__
combinations = list(it.product(args.resolution, args.algorithm, args.properties))

params = {}
stats  = {}

for i, combo in enumerate(combinations):
	r, a, p = combo

	params[combo] = {
		'additionalparams': ' '.join(['-p%s' % p, args.sequential]),
		'algorithm'       : a,
		'filepattern'     : 'leveling-resol:%d-alg:%s-props:%s%s-level:%%d' % (r, a, p, args.sequential),
		'paramrange'      : np.arange(1, int(np.log(r) / np.log(2))),
		'repeat'          : 100,
		'resolution'      : r, 
		'shapefunc'       : ridge,
		'withmemory'      : True,
	}

	print '  %2d/%2d: %r' % (i+1, len(combinations), combo)
	if not profile.checkFiles(**params[combo]):
		profile.profile(**params[combo])

	stats[combo] = profile.stats(**params[combo])

for res in args.resolution:
	rows = 1
	cols = len(args.algorithm) * len(args.properties)

	figure, axes = prf.subplots(rows, cols, aspect=1.0, mode='double', sharex=True, sharey=True)

	for j, (props, alg) in enumerate(it.product(args.properties, args.algorithm)):
		key = (res, alg, props)

		xvalues = params[key]['paramrange']

		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'R'       ) / 1000,                                            color=colors['R'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RI'      ) / 1000, profile.aggr(stats[key], 'R'     ) / 1000, color=colors['I'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIj'     ) / 1000, profile.aggr(stats[key], 'RI'    ) / 1000, color=colors['j'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJ'    ) / 1000, profile.aggr(stats[key], 'RIj'   ) / 1000, color=colors['J'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJf'   ) / 1000, profile.aggr(stats[key], 'RIjJ'  ) / 1000, color=colors['f'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfF'  ) / 1000, profile.aggr(stats[key], 'RIjJf' ) / 1000, color=colors['F'])
		axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, profile.aggr(stats[key], 'RIjJfF') / 1000, color=colors['M'])

		axes[j].plot(xvalues, profile.aggr(stats[res, alg, 'late'], 'RIjJfF' ) / 1000, 'k--')

		if props == 'late':
			axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMP' ) / 1000, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, color=colors['P'])
		# 	axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMPO') / 1000, profile.aggr(stats[key], 'RIjJfFMP') / 1000, color=colors['O'])
		# else:
		# 	axes[j].fill_between(xvalues, profile.aggr(stats[key], 'RIjJfFMO' ) / 1000, profile.aggr(stats[key], 'RIjJfFM' ) / 1000, color=colors['O'])

		axes[j].set_ylim(*ylim[res])
		axes[j].grid()

		# if props == 'yes':
		# 	axes[j].set_title('Early %s' % alg.upper(), fontsize=10)
		# elif props == 'no':
		# 	axes[j].set_title('%s' % alg.upper(), fontsize=10)
		# elif props == 'late':
		#	axes[j].set_title('Late %s' % alg.upper(), fontsize=10)

		if j == 0:
			axes[j].set_ylabel('Performance (ms)')

	# axes[0].set_xlabel('Ridge level')

	# axes[0].fill_between([], [], color=colors['J'], label='Later scan/global merge')
	# axes[0].fill_between([], [], color=colors['j'], label='First scan/global merge')
	# axes[0].fill_between([], [], color=colors['I'], label='Prelabel/local merge')
	# axes[0].fill_between([], [], color=colors['F'], label='Later analysis/path compression')
	# axes[0].fill_between([], [], color=colors['f'], label='First analysis/path compression')
	# axes[0].fill_between([], [], color=(0,0,0,0)  , label='  ')
	# axes[0].fill_between([], [], color=colors['O'], label='Overhead')
	# axes[0].fill_between([], [], color=colors['P'], label='Properties sum (late only)')
	# axes[0].fill_between([], [], color=colors['M'], label='Memset (not in early LE)')
	# legend = axes[0].legend(loc='upper left', bbox_to_anchor=(0, -0.3), ncol=3)

	prf.savefig(
		figure,
		'profile/out/plots/leveling%s-resol:%d' % (args.sequential, res),
	# 	legend,
	)
	plt.close(figure)

linefmt = '    %20r | %4s : %4s | %4.1f | %4.1f | %4.1f'
headfmt = '    %20s | %4s : %4s | %4s | %4s | %4s'
header = headfmt % ('Parameters', 'p1', 'p0', 'min', 'max', 'avg')

print '  Resolution ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.properties, args.algorithm))
for combo in combinations:
	p, a = combo
	r0 = args.resolution[0]
	for r1 in args.resolution[1:]:
		y0 = profile.aggr(stats[r0, a, p], 'OIjJFMP')
		y1 = profile.aggr(stats[r1, a, p], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, r1, r0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

print '  Algorithm ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.resolution, args.properties))
for combo in combinations:
	r, p = combo
	a0 = args.algorithm[0]
	for a1 in args.algorithm[1:]:
		y0 = profile.aggr(stats[r, a0, p], 'OIjJFMP')
		y1 = profile.aggr(stats[r, a1, p], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, a1, a0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

print '  Propsmodes ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.resolution, args.algorithm))
for combo in combinations:
	r, a = combo
	p0 = args.properties[0]
	for p1 in args.properties[1:]:
		y0 = profile.aggr(stats[r, a, p0], 'OIjJFMP')
		y1 = profile.aggr(stats[r, a, p1], 'OIjJFMP')
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, p1, p0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))
