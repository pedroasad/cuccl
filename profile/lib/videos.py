# coding: utf-8

import code
import copy
import itertools as it
import profile as prf
import matplotlib.pyplot as plt
import numpy as np
from profile.image import aggr
import profile.video as profile
import instancing_vid as pri
import scipy.interpolate as sci
import sys
# import scaling as prs
# import instancing as pri

from profile import args

print 'Profiling %s' % __file__

videos = """\
bear
blackswan
bmx-bumps
bmx-trees
boat
breakdance
breakdance-flare
bus
camel
car-roundabout
car-shadow
car-turn
cows
dance-jump
dance-twirl
dog-agility
dog
drift-chicane
drift-straight
drift-turn
elephant
flamingo
goat
hike
hockey
horsejump-high
horsejump-low
kite-surf
kite-walk
libby
lucia
mallard-fly
mallard-water
motocross-bumps
motocross-jump
motorbike
paragliding
paragliding-launch
parkour
rhino
rollerblade
scooter-black
scooter-gray
soapbox
soccerball
stroller
surf
swing
tennis
train\
""".split()

sequential = (len(sys.argv) > 1) and sys.argv[1] or ''

combinations = list(it.product(args.resolution, videos, args.algorithm, args.properties))

prefix = "data/davis/CuCCL/1080p/"

params = { key: {} for key in combinations }
stats  = { key: {} for key in combinations }

for i, key in enumerate(combinations):
	(res, vid, alg, pro) = key

	params[key] = {
		'additionalparams': '-p%s ' % pro + sequential,
		'algorithm'		  : alg,
		'filepattern'	  : 'video-%s-resol:%s-alg:%s-props:%s%s' % (vid, res, alg, pro, args.sequential),
		'resolution'	  : res, 
		'videopath'		  : '%s/%s.avi' % (prefix, vid),
		'withmemory'	  : True,
	}

	print '  %2d/%2d: %r' % (i+1, len(combinations), key)
	if not profile.checkFiles(params[key]['filepattern']):
		profile.profile(**params[key])

	stats[key] = profile.stats(**params[key])

newstats = {
	(r, a, p) : {
		key : np.hstack([stats[r, v, a, p][key] for v in videos])
		for key in profile.keys1d
	} for (r, a, p) in it.product(args.resolution, args.algorithm, args.properties)
}
for (r, a, p) in newstats:
	newstats[r, a, p].update({
		key : np.hstack([np.hstack(stats[r, v, a, p][key]) for v in videos])
		for key in profile.keys2d
	})

stats = newstats
ylim = {
	1024: (0, 3),
	2048: (0, 13),
	4096: (0, 40),
}

for res in args.resolution:
	# ==============================================================================
	# Execution time scatter plots (colored by number of iterations)
	# ==============================================================================
	rows = 1
	cols = len(args.algorithm) * len(args.properties)

	colors	= ['#c22326', '#f37338', '#fdb632', '#027878', '#801638']
	pointsize=4
	alpha=0.3

	# =========================================================================
	# Scatter plot of execution time as a function of foreground occupancy
	# =========================================================================
	figure, axes = prf.subplots(rows, cols, mode='double', aspect=1.0, sharex=True, sharey=True)
	for j, (props, alg) in enumerate(it.product(args.properties, args.algorithm)):
		for itr in range(1, 6):
			ref = (res, 'le', props)
			key = (res, alg , props)

			mask = (stats[ref]['iterations'] == itr)

			# if props == 'yes':
			# 	axes[j].set_title('Early %s' % alg.upper(), fontsize=10)
			# elif props == 'no':
			# 	axes[j].set_title('%s' % alg.upper(), fontsize=10)
			# elif props == 'late':
			# 	axes[j].set_title('Late %s' % alg.upper(), fontsize=10)

			mask = np.bitwise_and(mask, stats[key]['fg'] / res**2 <= 0.2)
			xvals = 100 * stats[key]['fg'][mask] / res**2
			yvals = stats[key]['compute2'][mask] / 1000
			axes[j].scatter(xvals, yvals, pointsize, colors[itr-1], alpha=alpha, marker='o', edgecolor='none', label='%d iter.' % itr)

			axes[j].set_ylim(*ylim[res])
			axes[j].grid()

	axes[0].set_ylabel(r'Total processing time (ms)')
	# axes[0].set_xlabel(r'\% of foreground')
	axes[0].set_xlim(0, 20)

	prf.savefig(
		figure,
		'profile/out/plots/videos-resol:%d' % res, 
		onlypdf=True,
	)
	plt.close(figure)

	# =========================================================================
	# Scatter plot of estimated time deviation as a function of foreground occupancy
	# =========================================================================
	figure, axes = prf.subplots(rows, cols, mode='double', aspect=1.0, sharex=True, sharey=True)
	for j, (props, alg) in enumerate(it.product(args.properties, args.algorithm)):
		ref = (res, 'le', props)
		for itr in range(3, 5):
			if (res, alg, props, itr) in pri.params:
				key = (res, alg, props)

				mask = (stats[ref]['iterations'] == itr)
				mask = np.bitwise_and(mask, stats[key]['fg'] / res**2 <= 0.2)
				xvals = 100 * stats[key]['fg'][mask] / res**2
				yvals = stats[key]['compute2'][mask] / 1000

				xest = 25 * pri.params[res, alg, props, itr]['paramrange'] / 16.0
				yest = aggr(pri.stats[res, alg, props, itr], 'C') / 1000
				f = sci.interp1d(xest, yest, fill_value='extrapolate')

				axes[j].scatter(xvals, yvals - f(xvals), pointsize, alpha=alpha, color=colors[itr-1], edgecolor='none')

				# if props == 'yes':
				# 	axes[j].set_title('Early %s' % alg.upper(), fontsize=10)
				# elif props == 'no':
				# 	axes[j].set_title('%s' % alg.upper(), fontsize=10)
				# elif props == 'late':
				# 	axes[j].set_title('Late %s' % alg.upper(), fontsize=10)

				axes[j].set_xlim(0, 20)
				axes[j].set_ylim(-5, 10)

		axes[j].grid()
		mask = np.bitwise_or(stats[ref]['iterations'] == 3, stats[ref]['iterations'] == 4)
		mask = np.bitwise_and(mask, stats[key]['fg'] / res**2 <= 0.2)
		xvals = 100 * stats[key]['fg'][mask] / res**2
		yvals = stats[key]['compute2'][mask] / 1000

		xest = 25 * pri.params[res, alg, props, itr]['paramrange'] / 16.0
		yest = aggr(pri.stats[res, alg, props, itr], 'C') / 1000
		f = sci.interp1d(xest, yest, fill_value='extrapolate')

		mean = np.mean(yvals - f(xvals))
		dev = np.std(yvals - f(xvals))
		max = np.max(np.abs(yvals - f(xvals) - mean) / dev)
		print '%s %s %6.2f & %.2f & %.2f' % (alg, props, max, mean, dev)

	for itr in range(1, 6):
		if (res, props, alg, itr) in pri.params:
			axes[0].scatter([], [], pointsize, color=colors[itr-1], label='%d iter.' % itr)

	axes[0].set_ylabel(r'Prediction error(ms)')
	# axes[0].set_xlabel(r'\% of foreground')

	prf.savefig(
		figure,
		'profile/out/plots/videos_predict%s-resol:%d' % (args.sequential, res), 
		onlypdf=True,
	)
	plt.close(figure)

	# ============================================================================
	# Legend
	# ============================================================================
	figure, axes = prf.subplots(1, 1, mode='single', aspect=0.05, frameon=False)
	for itr in range(1, 6):
		axes.scatter([], [], pointsize, colors[itr-1], marker='o', edgecolor='none', label='%d iter.' % itr)
	axes.xaxis.set_visible(False)
	axes.yaxis.set_visible(False)
	axes.axis('off')
	legend = axes.legend(loc='upper left', scatterpoints=1, ncol=5)

	prf.savefig(
		figure,
		'profile/out/plots/videos-legend', 
		legend, 
		onlypdf=True,
	)
	plt.close(figure)

	# ==============================================================================
	# Histograms
	# ==============================================================================
	if key != 'no':
		figure, axes = prf.subplots(1, 4, mode='double', aspect=1.0)

		key = (res, 'le', props)

		axes[0].hist(100 * stats[key]['fg'] / res**2, normed=True)
		axes[0].set_xlabel(r'(a) Fg. occupancy (\%)')
		axes[0].grid()
		for tick in axes[0].get_xticklabels():
			tick.set_rotation(30)

		axes[1].hist(stats[key]['cccount'][stats[key]['cccount'] < 30], normed=True)
		axes[1].set_xlabel('(b) CC count')
		axes[1].grid()

		axes[2].hist(100 * stats[key]['ccsize'] / res**2, normed=True)
		axes[2].set_xlabel(r'(c) CC size (fg. \%)')
		axes[2].grid()
		for tick in axes[2].get_xticklabels():
			tick.set_rotation(30)

		axes[3].hist(stats[key]['iterations'], [0.5, 1.5, 2.5, 3.5, 4.5, 5.5], normed=True)
		axes[3].set_xlabel('(d) LE iterations')
		axes[3].grid()

		prf.savefig(
			figure,
			'profile/out/plots/videos_hist-resol:%d' % res
		)
		plt.close(figure)

linefmt = '    %20r | %4s : %4s | %4.1f | %4.1f | %4.1f'
headfmt = '    %20s | %4s : %4s | %4s | %4s | %4s'
header = headfmt % ('Parameters', 'p1', 'p0', 'min', 'max', 'avg')

print '  Resolution ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.algorithm, args.properties))
for combo in combinations:
	a, p = combo
	r0 = args.resolution[0]
	for r1 in args.resolution[1:]:
		y0 = stats[r0, a, p]['compute']
		y1 = stats[r1, a, p]['compute']
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, r1, r0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))

print '  Algorithm ratios:'
print header
print '    ' + '-' * (len(header) - 4) 
combinations = list(it.product(args.resolution, args.properties))
for combo in combinations:
	r, p = combo
	a0 = args.algorithm[0]
	for a1 in args.algorithm[1:]:
		y0 = stats[r, a0, p]['compute']
		y1 = stats[r, a1, p]['compute']
		n = min(len(y1), len(y0))
		y0 = y0[:n]
		y1 = y1[:n]
		print linefmt % (combo, a1, a0, np.min(y1 / y0), np.max(y1 / y0), np.mean(y1 / y0))
