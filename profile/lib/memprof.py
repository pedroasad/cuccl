# coding: utf-8
#
# CuCCL, a library for CUDA Connected Components Labeling
# Copyright (C) 2017 Pedro de Souza Asad
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or issues with the code or would like to contribute
# to development, visit <https://gitlab.com/psa-exe/cuccl/>.

import profile as prf
import matplotlib.pyplot as plt
import numpy as np
import os
import sys

DRIVER_BINARY = 'build/cuccl-memprof'
NVPROF_BINARY = '/usr/local/cuda/bin/nvprof'


def profile(sizeRange, repeat=1):
	times = np.zeros_like(sizeRange, dtype=np.float)

	for i, size in enumerate(sizeRange):
		driverPart = '%s %d %d' % (DRIVER_BINARY, size, repeat)
		nvprofPart = '%s -u ns --print-gpu-summary --csv %s' % (NVPROF_BINARY, driverPart)
		filterPart = '%s 2>&1 | tail -n +6 | cut -d, -f4' % nvprofPart
		output = os.popen(filterPart).read()
		times[i] = float(output)

		sys.stdout.write('Progress: %5.1f%%\r' % (100.0 * (i + 1) / len(sizeRange)))
		sys.stdout.flush()

	return times


if __name__ == '__main__':
	resolutions = [1024, 2048, 4096]
	repeat = 10
	components = np.arange(10, 1000, 10)

	for resol in resolutions:
		fig = prf.figure(1.0, 'single')

		size = 44 * resol**2
		time = profile([size], repeat)[0]
		band = size / time
		plt.axhline(band, linestyle='--', color='b', label='Peak (early properties)')

		size = 44 * components
		time = profile(size, repeat)
		band = size / time
		plt.plot(components, band, color='b', label='Late properties')

		plt.legend(loc='best')
		plt.xlabel('Number of components')
		plt.ylabel('Bandwidth (GB/s)')

		prf.savefig(fig, 'profile/out/plots/memprof-resol:%d' % resol)
