import csv
import cv2
import numpy as np
import os
import os.path as osp
import subprocess as sp
import sys

BINDIR   = os.getcwd() + '/build'
IMGDIR   = os.getcwd() + '/profile/out/img'
PROFDIR  = os.getcwd() + '/profile/out/prof'
DUMPDIR  = os.getcwd() + '/profile/out/dump'
PROFILER = '/usr/local/cuda/bin/nvprof'

if os.path.exists('.git') and os.path.isdir('.git'):
	for directory in [BINDIR, IMGDIR, PROFDIR, DUMPDIR]:
		if not osp.exists(directory):
			os.makedirs(directory)
else:
	print('This does not appear to be the source directory of the project')
	sys.exit(1)
	

def profPaths(filepattern, paramrange, **kwargs):
	return (PROFDIR + '/' + filepattern % param + '.csv' for param in paramrange)	
	
	
def dumpPaths(filepattern, paramrange, **kwargs):
	return (DUMPDIR + '/' + filepattern % param + '.csv' for param in paramrange)
	
	
def imgPaths(filepattern, paramrange, **kwargs):
	return (IMGDIR + '/' + filepattern % param + '.png' for param in paramrange)
	
	
def checkFiles(filepattern, paramrange, **kwargs):
	dumpPathIter = dumpPaths(filepattern, paramrange)
	profPathIter = profPaths(filepattern, paramrange)
	
	for profPath, dumpPath in zip(profPathIter, dumpPathIter):
		if not os.path.exists(profPath):
			return False
		if 'additionalparams' in kwargs and '-p' in kwargs['additionalparams'].split() and not os.path.exists(dumpPath):
			return False

	return True
	
	
def openCSV(csvPath):
	csvFile = open(csvPath)
	line = csvFile.readline()
	while line[:2] == '==':
		line = csvFile.readline()
	fields = line.strip().lower().replace('"', '').split(',')
	reader = csv.DictReader(csvFile, fields)
	reader.next()
	return reader
	

def checkBuildResolution(resolution):
	versionCommand = BINDIR + '/cuccl-driver --version'
	versionString = sp.check_output(versionCommand.split())
	resolutionString = versionString.split('\n')[2]
	buildResolution = [int(x) for x in resolutionString.split()[2:5:2]]
	return buildResolution == [resolution, resolution]
	

def profile(resolution, repeat, shapefunc, paramrange, filepattern, algorithm, withmemory=False, additionalparams=''):
	
	os.chdir(BINDIR)
	
	if not checkBuildResolution(resolution):
		print sp.check_output(('cmake .. -DIMAGE_WIDTH=%d -DIMAGE_HEIGHT=%d' % (resolution, resolution)).split())
		print sp.check_output('make -j8'.split())
	
	imgPathsIter  =  imgPaths(filepattern, paramrange)
	profPathsIter = profPaths(filepattern, paramrange)
	dumpPathsIter = dumpPaths(filepattern, paramrange)

	# Generate image, profile and compute totals for each parameter in the specified range
	for i, (param, imgpath, profpath, dumppath) in enumerate(zip(paramrange, imgPathsIter, profPathsIter, dumpPathsIter)):
		# Generate profiled image
		img = shapefunc(resolution, param)
		cv2.imwrite(imgpath, img)

		# Run profiler and generate csv output
		if withmemory:
			mode = 'prof'
		else:
			mode = 'prof-no-mem'

		if '-p' in additionalparams.split():
			runcommand = '%s/cuccl-driver -i %s -a%s -f %d -m%s -q --dump.csv=%s %s' % (BINDIR, imgpath, algorithm, repeat, mode, dumppath, additionalparams)
		else:
			runcommand = '%s/cuccl-driver -i %s -a%s -f %d -m%s -q %s' % (BINDIR, imgpath, algorithm, repeat, mode, additionalparams)
		profilecommand = '%s --csv -u us --print-gpu-trace --log-file %s %s' % (PROFILER, profpath, runcommand)
		os.system(profilecommand)
		
		print 'Profiling: %5.1f%% completed\r' % (100.0 * (i + 1) / (len(paramrange))),
		sys.stdout.flush()
	
	
def stats(filepattern, paramrange, repeat, mean=True, **kwargs):
	attributes = [
		'M', #memset
		'D', #memcpy D2H
		'U', #memcpy H2D
		'I', #init
		'j', #firstjoin
		'J', #laterjoin
		'f', #first flat
		'F', #later flat
		'R', #redo
		'P', #prop
		'i', #iteration
		'O', #overhead
		'C', #compute
	]
	
	# Empty statistics
	stats = { attr: np.zeros((len(paramrange), repeat)) for attr in attributes }
	#newstats = np.zeros((len(paramrange), repeat), [(attr, float) for attr in attributes])
	
	profPathsIter = profPaths(filepattern, paramrange)

	# Generate image, profile and compute totals for each parameter in the specified range
	for i, (param, profPath) in enumerate(zip(paramrange, profPathsIter)):

		frame = 0
		compute = 0
		computeBegin = 0
		computeEnd = 0
			
		for j, kernelLaunch in enumerate(openCSV(profPath)):
			name = kernelLaunch['name'].lower()
			time = float(kernelLaunch['duration'])
			
			compute += time

			if 'htod' in name:
				stats['U'][i, frame] += time

			if 'memcpy' not in name:
				stats['C'][i, frame] += time

			if 'computebegin' in name:
				compute = 0
				computeBegin = float(kernelLaunch['start'])

			if 'computeend' in name:
				computeEnd = float(kernelLaunch['start'])
			 	stats['O'][i, frame] = computeEnd - computeBegin - compute

			if 'memset' in name:
				stats['M'][i, frame] += time

			if 'prelabel' in name or 'local' in name:
				stats['I'][i, frame] += time

			if 'scan' in name or 'merge' in name:
				if stats['i'][i, frame] == 0:
					stats['j'][i, frame] += time
				else:
					stats['J'][i, frame] += time
				
			if 'analysis' in name or 'updateborders' in name:
				if stats['i'][i, frame] == 0:
					stats['f'][i, frame] += time
				else:
					stats['F'][i, frame] += time
				stats['i'][i, frame] += 1
				
			if 'thrust' in name:# or 'markroot' in name or 'relabel' in name:
				stats['R'][i, frame] += time

			if 'props' in name:
				stats['P'][i, frame] += time
				
			if 'frameend' in name:
				stats['D'][i, frame] = float(kernelLaunch['start']) - computeEnd
				frame += 1
				
	if mean:
		newstats = np.zeros(len(paramrange), [(attr, float) for attr in attributes])
		for attr in attributes:
			# stats[attr] = np.mean(stats[attr], axis=1)
			newstats[attr] = np.mean(stats[attr], axis=1)
	else:
		newstats = np.zeros(len(paramrange) * repeat, [(attr, float) for attr in attributes])
		for attr in attributes:
			# stats[attr] = stats[attr].ravel()
			newstats[attr] = stats[attr].ravel()

	#newstats.aggr = lambda data, *args:	data[list(args)].view(float).reshape((len(data), -1))
	#return stats

	#newstats['O'] -= aggr(newstats, 'SIjJFRP')

	return newstats


def aggr(data, idx):
	return np.sum(data[list(idx)].view(float).reshape((len(data), -1)), axis=1)

