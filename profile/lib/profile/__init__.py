"""

http://bkanuka.com/articles/native-latex-plots/
https://gist.github.com/bkanuka/10796230#file-figures-template-py
"""
import matplotlib as mpl
mpl.use('pgf')
mpl.rcParams.update({					# setup matplotlib to use latex for output
	"axes.labelsize": 8,				# LaTeX default is 10pt font.
	"figure.autolayout": True,
	"font.family": 'serif',
	"font.monospace": [],
	"font.sans-serif": [],
	"font.size": 8,
	"font.serif": [],					# blank entries should cause plots to inherit fonts from the document
	"legend.fontsize": 8,				# Make the legend/label fonts a little smaller
	"pgf.preamble": [
		r"\usepackage[utf8x]{inputenc}",	# use utf8 fonts becasue your computer can handle it :)
		r"\usepackage[T1]{fontenc}",		# plots will be generated using this preamble
	],
	"pgf.texsystem": "lualatex",		# change this if using xetex or lautex
	"text.usetex": True,
	"text.usetex": True,				# use LaTeX to write all text
	"xtick.labelsize": 8,
	"ytick.labelsize": 8,
})
import matplotlib.pyplot as plt
from argparse import ArgumentParser


GOLDEN_RATIO = 0.6180339887498949

colors = {
	'O': '#aaaaaa',
	'I': '#1f78b4',
	'j': '#b2df8a',
	'J': '#33a02c',
	'f': '#fb9a99',
	'F': '#e31a1c',
	'M': '#fdbf6f',
	'P': '#ff7f00',
	'R': '#9457d1',
}
ylim = {
	1024: (0, 3),
	2048: (0, 13),
	4096: (0, 40),
}

parser = ArgumentParser(description='')
parser.add_argument('-a', '--algorithm', type=str, nargs='+', choices=['le', 'tm'], default=['le', 'tm'])
parser.add_argument('-l', '--level', type=int, nargs='+', default=[3])
parser.add_argument('-p', '--properties', type=str, nargs='+', choices=['late', 'yes'], default=['late', 'yes'])
parser.add_argument('-r', '--resolution', type=int, nargs='+', default=[4096])
parser.add_argument('-s', '--sequential', action='store_const', const='-s', default='')
args = parser.parse_args()


def figure(scale=1.0, mode='single', aspect=GOLDEN_RATIO, **kwargs):
	"""Creates a figure.

	Parameters
	----------
	**kwargs :
		Extra parameters to the :func:`matplotlib.pyplot.figure` function.

	See :func:`figsize` for other parameters
	"""
	kwargs.update({	'figsize': figsize(scale, mode, aspect) })
	return plt.figure(**kwargs)


def figsize(scale=1.0, mode='single', aspect=GOLDEN_RATIO):
	"""Calculates figure sizes.

	Parameters
	----------
	scale : number (0-1)
		Width of figure, as a fraction of the text column.

	mode : ``'single'`` or ``'double'``
		Whether it's a single or double-column figure.

	aspect : number
		Aspect ratio (height / width) of figure.

	Notes
	-----
	A good choice for the aspect ratio when multiple plots are present in the
	figure is ``GOLDEN_RATIO`` multiplied by the ratio between the number of
	rows and the number columns.
	"""

	# These measures were obtained with the \the\linewidth LaTeX command for
	# the svjour2.cls Springer-Verlag journal template
	line_width = {
		'single': 244.30945, # Line width in single column mode
		'double': 500.0,	 # Line width in double column mode
	}

	fig_width_pt = line_width[mode] # Get this from LaTeX using \the\textwidth
	inches_per_pt = 1.0/72.27       # Convert pt to inch

	fig_width = fig_width_pt * inches_per_pt * scale # width in inches
	fig_height = fig_width * aspect                  # height in inches

	return (fig_width,fig_height)


def subplots(rows, cols, scale=1.0, mode='single', aspect=GOLDEN_RATIO, **kwargs):
	"""Creates a figure and related axes.

	Parameters
	----------
	rows : integer
		Number of suplot rows.

	cols : integer
		Number of subplot cols.

	**kwargs :
		Extra parameters to the :func:`matplotlib.pyplot.subplots` function.

	See :func:`figsize` for other parameters
	"""
	kwargs.update({	'figsize': figsize(scale, mode, 1.0 * rows * aspect / cols) })
	return plt.subplots(rows, cols, **kwargs)


def savefig(figure, filename, *extra_artists, **kwargs):
	"""Saves a figure.

	Parameters
	----------
	figure : :class:`matplotlib.figure.Figure` instance

	filename : string

	*extra_artists : 
		list of :class:`matplotlib.artist.Artist` instances Extra elements such
		as legends that should be accounted for when automatically adjusting
		the layout.
	"""
	if 'onlypdf' in kwargs and not kwargs['onlypdf']:
		figure.savefig('%s.pgf' % filename,
			bbox_extra_artists=extra_artists,
			bbox_inches='tight'
		)
	
	figure.savefig('%s.pdf' % filename,
		bbox_extra_artists=extra_artists,
		bbox_inches='tight'
	)

