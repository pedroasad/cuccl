"""
"""
import cv2
import itertools as it
import numpy as np


def spiral(resol, segments, direction='out'):
	def draw(image, order, fg):
		"""Spiral of order segments in a square image of resol x resol pixels.
		"""
		patch = image
		for i in range(order):
			if i % 4 == 0:
				patch[0, :] = fg
				if i > 0:
					patch = patch[:, 2:]
			elif i % 4 == 1:
				patch[:, -1] = fg
				patch = patch[2:, :]
			elif i % 4 == 2:
				patch[-1, :] = fg
				patch = patch[:, :-2]
			else:
				patch[:, 0] = fg
				patch = patch[:-2, :]

	image = np.zeros((resol, resol), dtype=np.uint8)

	if direction == 'in':
		draw(image, segments, 255)
	elif direction == 'out':
		draw(image, resol - 1, 255)
		draw(image, resol - 1 - segments, 0)
				
	return image


def ridge(size, levels):
	"""A ridge of size/2+1 pixels width with levels divisions in a size x size
	image.
	"""
	def crest(levels):
		size = (levels, 2**(levels-1)+1)
		image = np.zeros(size, np.uint8)
		periods = [2**l for l in range(levels)]
		for i, p in enumerate(reversed(periods)):
			image[i:, ::p] = 255
		return image
	
	image = np.zeros((size, size))
	top = crest(levels)
	th, tw = top.shape
	if tw >= size / 2 + 1:
		raise ValueError('Cannot make a level %d crisp with only %dx%d pixels' % (levels, size, size))
	th2 = 2 * th
	tw2 = size / 2 + 1
	image[:th2, :tw2] = cv2.resize(top, (tw2, th2), interpolation=cv2.INTER_NEAREST)
	for col, row in enumerate(range(th2, th2 + tw2)):
		image[row, col:(col+tw2)] = 255
	return image


def scaleridge(levels):
	def function(resol, scale):
		image = np.zeros((resol, resol), np.uint8)
		pattern = ridge(resol, levels)
		y, x = np.nonzero(pattern)
		h, w = np.max(y), np.max(x)
		pattern = pattern[:h, :w]
		pattern = cv2.resize(pattern, (int(w * scale), int(h * scale)), interpolation=cv2.INTER_NEAREST)
		h, w = pattern.shape
		image[:h, :w] = pattern
		return image
	return function


def multiridge_n(levels, divisions):
	"""Returns a function f(r, n) that produces a ridge shape with 
	``ridge(r, levels)`` and divides it in 4**d smaller copies and keeps only 
	the first k copies.
	"""
	def function(resol, figures):
		return splitridge(levels, resol, divisions, figures)
	return function
	
	
def multiridge(levels):
	"""Returns a function f(r, d) that produces a ridge shape with 
	``ridge(r, levels)`` and divides it in 4**d smaller copies that sum up the
	same number of pixels of the original shape.
	"""
	def function(resol, divisions):
		return splitridge(levels, resol, divisions, 4**divisions)
	return function
	
	
def splitridge(levels, resol, divisions, figures):
	"""A ridge of ``size/2+1`` pixels width with levels divisions in a ``size``
	by ``size`` image is evenly divided ``divisions`` times both horizontally 
	and vertically and then only the ``figures`` first instances (top to 
	bottom, left to right) are kept.
	"""
	image = np.zeros((resol, resol), np.uint8)
	size = resol / 2**divisions
	crisp = ridge(size-2, levels)
	h, w = crisp.shape
	relevant = np.nonzero(crisp)
	cw = (size - 2) / 2 + 1
	ch = 2 * levels + cw
	rows = range(0, 2**divisions * (ch + 2), (ch + 2))
	cols = range(0, 2**divisions * (cw + 2), (cw + 2))
	pairs = it.product(reversed(rows), reversed(cols))
	for k, (i, j) in zip(range(figures), pairs):
		image[i:(i+h), j:(j+w)][relevant] = crisp[relevant]
	return image

