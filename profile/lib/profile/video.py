import csv
import cv2
import numpy as np
import os
import os.path as osp
import subprocess as sp
import sys

BINDIR	 = os.getcwd() + '/build'
PROFDIR  = os.getcwd() + '/profile/out/prof'
DUMPDIR  = os.getcwd() + '/profile/out/dump'
PROFILER = '/usr/local/cuda/bin/nvprof'

if os.path.exists('.git') and os.path.isdir('.git'):
	for directory in [BINDIR, PROFDIR, DUMPDIR]:
		if not osp.exists(directory):
			os.makedirs(directory)
else:
	print('This does not appear to be the source directory of the project')
	sys.exit(1)
	

def profPath(filepattern):
	return PROFDIR + '/' + filepattern + '.csv'
	

def dumpPath(filepattern):
	return DUMPDIR + '/' + filepattern + '-dump.csv'
	
	
def checkFiles(filepattern):
	dumppath = dumpPath(filepattern)
	profpath = profPath(filepattern)
	
	if not os.path.exists(profpath) or not os.path.exists(dumppath):
		return False

	return True
	
	
def openCSV(csvPath):
	csvFile = open(csvPath)
	line = csvFile.readline()
	while line[:2] == '==':
		line = csvFile.readline()
	fields = line.strip().lower().replace('"', '').split(',')
	reader = csv.DictReader(csvFile, fields)
	reader.next()
	return reader
	

def checkBuildResolution(resolution):
	versionCommand = BINDIR + '/cuccl-driver --version'
	versionString = sp.check_output(versionCommand.split())
	resolutionString = versionString.split('\n')[2]
	buildResolution = [int(x) for x in resolutionString.split()[2:5:2]]
	return buildResolution == [resolution, resolution]
	

def profile(resolution, videopath, filepattern, algorithm, withmemory=False, additionalparams=''):
	
	if not checkBuildResolution(resolution):
		os.chdir(BINDIR)
		print sp.check_output(('cmake .. -DIMAGE_WIDTH=%d -DIMAGE_HEIGHT=%d' % (resolution, resolution)).split())
		print sp.check_output('make -j8'.split())
		os.chdir('..')
	
	profpath = profPath(filepattern)
	dumppath = dumpPath(filepattern)

	# Run profiler and generate csv output
	if withmemory:
		mode = 'prof'
	else:
		mode = 'prof-no-mem'

	if '-pyes' in additionalparams or '-plate' in additionalparams:
		runcommand = '%s/cuccl-driver -i %s -a%s -m%s --dump.csv=%s %s' % (BINDIR, videopath, algorithm, mode, dumppath, additionalparams)
	else:
		runcommand = '%s/cuccl-driver -i %s -a%s -m%s %s' % (BINDIR, videopath, algorithm, mode, additionalparams)
	profilecommand = '%s --csv -u us --print-gpu-trace --log-file %s %s' % (PROFILER, profpath, runcommand)
	print 'Profiling: "%s"' % profilecommand
	os.system(profilecommand)
	

keys1d = [
	'memcpy', 'memset', 'init', 'join', 'joincum', 'firstjoin', 'laterjoin',
	'flat', 'flatcum', 'redo', 'redocum', 'prop', 'propcum', 'compute', 'compute2',
	'iterations', 'cccount', 'fg'
]

keys2d = ['ccsize']
	
def stats(**kwargs):
	attributes = keys1d[:-2]
	
	# Empty statistics
	stats = { attr: [0.0] for attr in attributes }
	profpath = profPath(kwargs['filepattern'])
		
	for kernelLaunch in openCSV(profpath):
		name = kernelLaunch['name'].lower()
		time = float(kernelLaunch['duration'])

		if 'memcpy' in name:
			stats['memcpy'][-1] += time
		else:
			stats['compute2'][-1] += time

		if 'prelabel' in name or 'local' in name:
			stats['compute'][-1] += time
			stats['init'   ][-1] += time
			stats['joincum'][-1] += time
			stats['flatcum'][-1] += time
			stats['redocum'][-1] += time
			stats['propcum'][-1] += time

		if 'scan' in name or 'merge' in name:
			if stats['iterations'][-1] == 0:
				stats['firstjoin'][-1] += time
			else:
				stats['laterjoin'][-1] += time
			stats['compute'][-1] += time
			stats['join'   ][-1] += time
			stats['joincum'][-1] += time
			stats['flatcum'][-1] += time
			stats['redocum'][-1] += time
			stats['propcum'][-1] += time
			
		if 'analysis' in name or 'update' in name:
			stats['iterations'][-1] += 1
			stats['compute'][-1] += time
			stats['flat'   ][-1] += time
			stats['flatcum'][-1] += time
			stats['redocum'][-1] += time
			stats['propcum'][-1] += time
			
		if 'thrust' in name or 'markroot' in name or 'relabel' in name:
			stats['compute'][-1] += time
			stats['redo'   ][-1] += time
			stats['redocum'][-1] += time
			stats['propcum'][-1] += time
		
		if 'memset' in name:
			stats['compute'][-1] += time
			stats['memset' ][-1] += time
			stats['propcum'][-1] += time

		if 'props' in name:
			stats['compute'][-1] += time
			stats['prop'   ][-1] += time
			stats['propcum'][-1] += time
			
		if 'frameend' in name:
			for attr in attributes:
				stats[attr].append(0.0)
				
	for attr in attributes:
		stats[attr] = np.array(stats[attr][:-1])

	stats['ccsize']  = [[] for f in stats['compute']]
	stats['cccount'] = np.zeros_like(stats['compute'])
	stats['fg']      = np.zeros_like(stats['compute'], dtype=np.float)

	if 'additionalparams' in kwargs and '-pyes' in kwargs['additionalparams'] or '-plate' in kwargs['additionalparams']:
		dumpfile = open(dumpPath(kwargs['filepattern']))
		dumpfile.readline()
		for i, line in enumerate(dumpfile):
			if line:
				sizes = [int(x) for x in line.rstrip().split(',') if x]

				stats['ccsize'][i-1].extend(sizes)
				stats['cccount'][i-1] += len(sizes)
				stats['fg'][i-1] += sum(sizes)
		
	return stats

