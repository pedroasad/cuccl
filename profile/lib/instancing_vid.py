# coding: utf-8

import copy
import itertools as it
import numpy as np
import profile as prf
import matplotlib.pyplot as plt
import profile.image as profile

from profile import args, colors, ylim
from profile.shapes import multiridge_n

# =============================================================================
# Single Ridge Pattern with increasing level
# =============================================================================
print 'Profiling %s' % __file__
args.level = [3, 4]

combinations = list(it.product(args.resolution, args.algorithm, args.properties, args.level))

params = {}
stats  = {}

for i, combo in enumerate(combinations):
	r, a, p, l = combo

	subdivisions = 2
	step = 1

	params[combo] = {
		'additionalparams': ' '.join(['-p%s' % p, args.sequential]),
		'algorithm'       : a,
		'filepattern'     : 'instancing-resol:%d-alg:%s-props:%s%s-level:%d-subdiv:%d-inst:%%d' % (r, a, p, args.sequential, l, subdivisions),
		'paramrange'      : np.arange(step, 4**subdivisions+1, step),
		'repeat'          : 100,
		'resolution'      : r, 
		'shapefunc'       : multiridge_n(l, subdivisions),
		'withmemory'      : True,
	}

	print '  %2d/%2d: %r' % (i+1, len(combinations), combo)
	if not profile.checkFiles(**params[combo]):
		profile.profile(**params[combo])

	stats[combo] = profile.stats(**params[combo])
