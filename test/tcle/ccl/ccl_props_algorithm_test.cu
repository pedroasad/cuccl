#include <gtest/gtest.h>

#include <string.h>

#include <tcle/core/ccl.h>
#include <tcle/core/wrappers.h>
#include <tcle/core/image.h>
#include <tcle/core/utils.h>
#include <tcle/core/vector.h>
#include <tcle/ccl/tcle.h>
#include <tcle/meta/fit_threads.h>
#include <tcle/meta/op_traits.h>

namespace lcg {
namespace tcle {
namespace testing {

class ccl_props_algorithm_Test :
		public ::testing::TestWithParam<rectangle> {
};

/**
 * Tests on rects aligned with the xy axes.
 */
TEST_P(ccl_props_algorithm_Test, Aligned_rect) {
	typedef fit_threads<label_image::width, label_image::height> Layout;

	rectangle rect = GetParam();
	const int x = rect.x, y = rect.y;

	pinned<binary_image> binary;
	binary->fill(binary_image::BACKGROUND);
	draw(*binary, rect, binary_image::FOREGROUND);

	ccl_props_algorithm *labeler = new tcle::label_equivalence_props(Layout::grid, Layout::block);

	ccl_props_algorithm::labels_and_props result = labeler->cclprops(binary);
	hosted<label_image> labels = result.first;
	hosted<label_image::prop_table> props = result.second;
	cudaDeviceSynchronize();

	const label_image::type replabel = (x + y * label_image::width);
	label_image refimg;
	refimg.fill(label_image::BACKGROUND);
	draw(refimg, rect, replabel);

	label_image::prop_table refprops;
	refprops.area.fill(0);
	refprops.centroid.fill(make_ulonglong2(0, 0));
	refprops.variance.fill(make_ulonglong3(0, 0, 0));

	int    area      = rect.area    ();
	ulonglong2 centroid  = rect.centroid();
	ulonglong2 variance  = rect.variance();

	refprops.area    (x, y)   = area;
	refprops.centroid(x, y)   = centroid * area;
	refprops.variance(x, y).x = variance.x * area + sqr(centroid.x);
	refprops.variance(x, y).y = variance.y * area + sqr(centroid.y);
	refprops.variance(x, y).z = centroid.x * centroid.y;

	// The two following assertion below compare large memory regions. If they fail
	// and you need more fine grained information, use the tests below them. Otherwise
	// leave them commented out
	ASSERT_EQ(0, memcmp(refimg.data, labels->data, sizeof(label_image)));
//	ASSERT_EQ(0, memcmp(&refprops, &props-> sizeof(label_image::prop_table)));

	for (int x = 0; x < label_image::width; x++)
		for (int y = 0; y < label_image::height; y++)
			if (x == rect.x and y == rect.y) {
				ASSERT_EQ      (refprops.area    (x, y)  , props->area    (x, y));

				ASSERT_FLOAT_EQ(refprops.centroid(x, y).x, props->centroid(x, y).x);
				ASSERT_FLOAT_EQ(refprops.centroid(x, y).y, props->centroid(y, y).y);

				ASSERT_FLOAT_EQ(refprops.variance(x, y).x, props->variance(x, y).x);
				ASSERT_FLOAT_EQ(refprops.variance(x, y).y, props->variance(y, y).y);
				ASSERT_FLOAT_EQ(refprops.variance(x, y).z, props->variance(y, y).z);
			} else {
				ASSERT_EQ      (0  , props->area    (x, y));

				ASSERT_FLOAT_EQ(0, props->centroid(x, y).x);
				ASSERT_FLOAT_EQ(0, props->centroid(y, y).y);

				ASSERT_FLOAT_EQ(0, props->variance(x, y).x);
				ASSERT_FLOAT_EQ(0, props->variance(y, y).y);
				ASSERT_FLOAT_EQ(0, props->variance(y, y).z);
			}
}

/**
 * \brief What happens when the source img is completely blank? The resulting
 * img should also be and the properties table should be filled with zeroes.
 */
//INSTANTIATE_TEST_CASE_P(Whole_img_is_background, ccl_props_algorithm_Test, ::testing::Values(rectangle(0, 0)));

/**
 * What if the img is all foreground? This case is good for testing the
 * properties' data types' capacity to represent very large objects.
 */
//INSTANTIATE_TEST_CASE_P(Whole_img_is_foreground, ccl_props_algorithm_Test, ::testing::Values(label_image::domain));

/**
 * Tests on a variety of rect sizes.
 */
//INSTANTIATE_TEST_CASE_P(Arbitrary_rects, ccl_props_algorithm_Test, ::testing::Values(
//		rectangle(label_image::width / 2, label_image::height / 2, label_image::height / 4, label_image::height / 4)
//));

} /* namespace testing */
} /* namespace tlce*/
} /* namespace lcg */
