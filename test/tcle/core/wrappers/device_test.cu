#include <tcle/core/wrappers.h>
#include <gtest/gtest.h>

namespace {

template <typename Type>
__global__ void cmpeq(const Type *address, Type value, bool *flag) {
	if (address[threadIdx.x] != value)
		*flag = false;
}

template <typename Type>
__global__ void cmpeq(const Type *address, const Type *values, bool *flag) {
	if (address[threadIdx.x] != values[threadIdx.x])
		*flag = false;
}

}

namespace lcg {
namespace tcle {
namespace testing {

template <typename Type>
class device_test : public ::testing::Test {
};

typedef ::testing::Types<int> types;
TYPED_TEST_CASE(device_test, types);

TYPED_TEST(device_test, has_empty_constructor) {
	device<TypeParam> variable;
	SUCCEED();
}

TYPED_TEST(device_test, copy_constructible_from_TypeParam) {
	const TypeParam expected = 42;

	device<TypeParam> variable = expected;

	hosted  <TypeParam> result   = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, copy_constructible_from_hosted) {
	const TypeParam expected = 42;
	hosted<TypeParam> other = expected;

	device<TypeParam> variable = expected;

	hosted  <TypeParam> result   = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, copy_constructible_from_pinned) {
	const TypeParam expected = 42;
	pinned<TypeParam> other = expected;

	device<TypeParam> variable = other;

	hosted  <TypeParam> result   = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, copy_constructible_from_device) {
	const TypeParam expected = 42;
	device<TypeParam> buffer   = expected;

	device<TypeParam> variable = buffer;

	hosted  <TypeParam> result   = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, copy_constructible_from_device_ref) {
	const TypeParam expected = 42;
	device<TypeParam> buffer     = expected;
	device_ref<TypeParam> buffer_ref = buffer;

	device<TypeParam> variable = buffer_ref;

	hosted  <TypeParam> result   = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, assignable_from_TypeParam) {
	const TypeParam expected = 42, wrong = 33;
	device<TypeParam> variable = wrong;

	variable = expected;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, assignable_from_hosted) {
	const TypeParam expected = 42, wrong = 33;
	hosted<TypeParam> other = expected;
	device<TypeParam> variable = wrong;

	variable = other;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, assignable_from_pinned) {
	const TypeParam expected = 42, wrong = 33;
	pinned<TypeParam> other = expected;
	device<TypeParam> variable = wrong;

	variable = other;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, assignable_from_device) {
	TypeParam wrong = 33, expected = 42;
	device<TypeParam> variable = wrong;
	device<TypeParam> other    = expected;

	variable = other;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

TYPED_TEST(device_test, assignable_from_device_ref) {
	TypeParam wrong = 33, expected = 42;
	device<TypeParam> variable = wrong;
	device<TypeParam> buffer   = expected;
	device_ref<TypeParam> other    = buffer;

	variable = other;

	hosted<TypeParam> result = variable;
	CUDA_FATAL( cudaDeviceSynchronize() );
	ASSERT_EQ(expected, result);
}

} /* namespace testing */
} /* namespace tcle */
} /* namespace lcg */
