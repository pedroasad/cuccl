CuCCL
=====

[![license-shield](https://img.shields.io/badge/license-GPL3.0-blue.svg)](https://www.gnu.org/licenses/gpl-3.0-standalone.html)
![version-shield](https://img.shields.io/badge/version-0.1-orange.svg)

[![pipeline status](https://gitlab.com/lcg/cuccl/badges/master/pipeline.svg)](https://gitlab.com/lcg/cuccl/commits/master)
[![coverage report](https://gitlab.com/lcg/cuccl/badges/master/coverage.svg)](https://gitlab.com/lcg/cuccl/commits/master)

![cuda-shield](https://img.shields.io/badge/CUDA-9-blue.svg)
![c++-shield](https://img.shields.io/badge/C++-11-blue.svg)
![Python-shield](https://img.shields.io/badge/Python-2.7-blue.svg)

---

CuCCL is a C++/CUDA library that implements modified versions of two GPU connected component labeling algorithms for images: Hawick et al. 2010 [^1], and Stava et al. 2011 [^2].
It also contains Python scripts for evaluating the algorithms's performance using synthetic test patterns or (if available) segmented video sequences.
This library is being developed by students of [LCG](https://lcg.gitlab.io), the Computer Graphics Laboratory of the Federal University of Rio de Janeiro.


# Dependencies

C++ Dependencies for the library and command-line interface:

* Boost 1.5
* CMake 3.5 (for building)
* CUDA toolkit 9.1
* OpenCV 2.4

Python 2.7 dependencies for profiling and generating plots:

* Matplotlib 2.2
* Numpy 1.14
* Scipy 1.0

If you whish to build the documentation, you'll need Doxygen 1.8.


# Installation

Just type

```bash
mkdir build; cd build  
cmake ..  
make && make install
```

If CMake's `BUILD_DOCS` variable is true (meaning you want to build the docs), `make install` will fail.
If this happens, either set it to false, or type `make docs`, then `make install`.

__Note:__ In some cases, the default C/C++ compiler may be incompatible with the latest version of CUDA available in the repositories.
In this case, manually point to a compatible compiler using the `CUDA_HOST_COMPILER` CMake variable.


# Usage

Check the [documentation page](https://lcg.gitlab.io/cuccl) for information on the C++ API.
The Python profiling tools are currently undocumented, but we plan to change it soon.

__Note:__ The library is still in alpha version, and a lot of things may change at this point.
However, we expect to release a stable version sometime later during this year.


# References

[^1]: Hawick, Kenneth A., Arno Leist, and Daniel P. Playne. "Parallel graph component labelling with GPUs and CUDA." Parallel Computing 36.12 (2010): 655-678.
[^2]: Št, Ondřej, and Bedřich Beneš. "Connected component labeling in CUDA." GPU computing gems emerald edition. 2011. 569-581.
