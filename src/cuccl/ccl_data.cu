#include <cuccl/ccl_data.h>

namespace lcg {
namespace cuccl {

ccl_data ccl_data::allocDevice(const dim3 &size, bool withProps) {
	ccl_data frame;
	frame.size = size;
	CUTILS_SAFE( cudaMalloc((void**) &frame.labels, size.x * size.y * sizeof(label_t)) );
	if (withProps) {
		frame.ccSlots = size.x * size.y;
		CUTILS_SAFE( cudaMalloc((void**) &frame.area    , size.x * size.y * sizeof(    area_t)) );
		CUTILS_SAFE( cudaMalloc((void**) &frame.centroid, size.x * size.y * sizeof(centroid_t)) );
		CUTILS_SAFE( cudaMalloc((void**) &frame.variance, size.x * size.y * sizeof(variance_t)) );
	} else {
		frame.ccSlots = 0;
		frame.area     = nullptr;
		frame.centroid = nullptr;
		frame.variance = nullptr;
	}
	return frame;
}

ccl_data ccl_data::allocHost(const dim3 &size, bool withProps) {
	ccl_data frame;
	frame.size = size;
	frame.labels = new label_t[size.x * size.y];
	if (withProps) {
		frame.ccSlots = size.x * size.y;
		frame.area     = new     area_t[size.x * size.y];
		frame.centroid = new centroid_t[size.x * size.y];
		frame.variance = new variance_t[size.x * size.y];
	} else {
		frame.ccSlots = 0;
		frame.area     = nullptr;
		frame.centroid = nullptr;
		frame.variance = nullptr;
	}
	return frame;
}

void ccl_data::copyTo(ccl_data &other, cudaMemcpyKind transferKind) {
	// TODO: Review this command: it should be sync'd because of sequential relabels, but is this the right place?
	CUTILS_SAFE( cudaDeviceSynchronize() );
	CUTILS_SAFE( cudaMemcpy(other.labels, labels, size.x * size.y * sizeof(label_t), transferKind) );
	if (area != nullptr and other.area != nullptr) {
		other.ccSlots = ccSlots;
		CUTILS_SAFE( cudaMemcpy(other.area    , area    , ccSlots * sizeof(    area_t), transferKind) );
		CUTILS_SAFE( cudaMemcpy(other.centroid, centroid, ccSlots * sizeof(centroid_t), transferKind) );
		CUTILS_SAFE( cudaMemcpy(other.variance, variance, ccSlots * sizeof(variance_t), transferKind) );
	}
}

ccl_data ccl_data::withoutProps() {
	return { size, 0, labels, nullptr };
}

} /* namespace lcg::cuccl */
} /* namespace lcg */
