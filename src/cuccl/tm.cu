#include <cuccl/ccl_data.h>
#include <cuccl/internal/find_root.h>
#include <cuccl/internal/neighbors_8.h>
#include <cuccl/internal/neighbors_8_scan.h>
#include <cuccl/internal/relabel.h>
#include <cuccl/internal/smallest.h>
#include <cuccl/internal/transfer_props.h>
#include <cuccl/internal/le/analysis.h>
#include <cuccl/internal/le/scan.h>
#include <cuccl/internal/le/scan.h>
#include <cuccl/internal/lle/local_phase.h>
#include <cuccl/tm.h>
#include <cutils/vector.h>

using namespace lcg::cutils;

namespace lcg {
namespace cuccl {

__device__ __inline__
bool inRange(const cutils::vec2i &point, const dim3 &range) {
	return point.x >= 0 and point.y >= 0 and point.x < range.x and point.y < range.y;
}

__device__
void Union(label_t *labels, const cutils::vec2i &point1, const cutils::vec2i &point2, const dim3 &size, bool &changed) {
	if (inRange(point1, size) and inRange(point2, size)) {
		const basic_label label1 = labels[point1.x + point1.y * size.x];
		const basic_label label2 = labels[point2.x + point2.y * size.x];

		if (label1.isForeground() and label2.isForeground()) {
			const basic_label root1 = internal::findRoot<basic_label>(labels, label1);
			const basic_label root2 = internal::findRoot<basic_label>(labels, label2);

			if (root1 < root2) {
				labels[root2.address()] = root1;
				changed = true;
			}

			if (root2 < root1) {
				labels[root1.address()] = root2;
				changed = true;
			}
		}
	}
}

__global__
void MergeTiles(label_t *dLabelsData, int subBlockDim, const dim3 size) {
	__shared__ bool changed; // Used to check for convergence

	const cutils::vec2i subBlockId(
		blockIdx.x * blockDim.x + threadIdx.x,
		blockIdx.y * blockDim.y + threadIdx.y
	);
	const int repetitions = subBlockDim / blockDim.z;  // How many times one thread is reused in the respective sub-block

	do {
		__syncthreads();
		changed = false;
		__syncthreads();

		//process the bottom horizontal border
		for (int k = 0; k < repetitions; k++) {
			const cutils::vec2i xy(
				subBlockId.x * subBlockDim + threadIdx.z + k * blockDim.z,
				(subBlockId.y + 1) * subBlockDim - 1
			);

			Union(dLabelsData, xy, xy + cutils::vec2i(-1, 1), size, changed);
			Union(dLabelsData, xy, xy + cutils::vec2i( 0, 1), size, changed);
			Union(dLabelsData, xy, xy + cutils::vec2i( 1, 1), size, changed);
		}

		//process the right vertical border
		for (int k = 0; k < repetitions; k++) {
			const cutils::vec2i xy(
				(subBlockId.x + 1) * subBlockDim - 1,
				subBlockId.y * subBlockDim + threadIdx.z + k * blockDim.z
			);

			Union(dLabelsData, xy, xy + cutils::vec2i(1, -1), size, changed);
			Union(dLabelsData, xy, xy + cutils::vec2i(1,  0), size, changed);
			Union(dLabelsData, xy, xy + cutils::vec2i(1,  1), size, changed);
		}

		__syncthreads();
	} while (changed);
}

__global__
void UpdateBorders(label_t *dLabelsData, int subBlockDim, const dim3 size) {

	const cutils::vec2i subBlockId(
		blockIdx.x * blockDim.x + threadIdx.x,
		blockIdx.y * blockDim.y + threadIdx.y
	);
	const int repetitions = subBlockDim / blockDim.z;  // How many times one thread is reused in the respective sub-block

	//process the bottom horizontal border
	for (int k = 0; k < repetitions; k++) {
		const cutils::vec2i xy(
			subBlockId.x * subBlockDim + threadIdx.z + k * blockDim.z,
			(subBlockId.y + 1) * subBlockDim - 1
		);
		const int I = internal::image_indexing::globalOffset(xy);

		const basic_label label = dLabelsData[I];

		if (label.isForeground())
			dLabelsData[I] = internal::findRoot<basic_label>(dLabelsData, label);
	}

	//process the right vertical border
	for (int k = 0; k < repetitions; k++) {
		const cutils::vec2i xy(
			(subBlockId.x + 1) * subBlockDim - 1,
			subBlockId.y * subBlockDim + threadIdx.z + k * blockDim.z
		);

		const int I = internal::image_indexing::globalOffset(xy);

		const basic_label label = dLabelsData[I];

		if (label.isForeground())
			dLabelsData[I] = internal::findRoot<basic_label>(dLabelsData, label);
	}
}

TileMerging::Options::Options() :
		computePropertiesLater   (false),
		sequentialRelabel        (true ),
		tileSize(16),
		blockFactor(16) {
}

TileMerging::TileMerging(const dim3 &size, Options options) :
			options(options), size(size), seqlabels(nullptr) {

	if (options.sequentialRelabel)
		CUTILS_SAFE( cudaMalloc((void**) &seqlabels, size.x * size.y * sizeof(label_t)) );

	cudaDeviceProp props;
	CUTILS_SAFE( cudaGetDeviceProperties(&props, 0) );
	maxThreadsPerBlock = props.maxThreadsPerBlock;
}

TileMerging::~TileMerging() {
	if (seqlabels != nullptr)
		CUTILS_SAFE( cudaFree(seqlabels) );
}

void TileMerging::ccl(const segment_t *binary, ccl_data &output) {
	ccl_data frame;
	if (output.hasProps() and options.computePropertiesLater)
		frame = output.withoutProps();
	else
		frame = output;

	// Prelabel
	// TODO: Must be able to control thread layout in this phase
	internal::lle::localPhase(size, binary, frame, dim3(options.tileSize, options.tileSize));

	// Tile merging
	int tileSize = options.tileSize;
	int blockFactor = options.blockFactor;
	//TODO: Are the code range verifications in Union enough to guarantee any image size will work?
	while (true) {
		blockFactor = min(blockFactor, size.x / tileSize);

		dim3 block(
			blockFactor,
			blockFactor,
			min(tileSize, maxThreadsPerBlock / (blockFactor * blockFactor))
		);
		dim3 grid (
			size.x / (blockFactor * tileSize),
			size.y / (blockFactor * tileSize)
		);

		MergeTiles<<<grid, block>>>(frame.labels, tileSize, size);

		if (grid.x <= 1 and grid.y <= 1 and grid.z <= 1)
			break;

		UpdateBorders<<<grid, block>>>(frame.labels, tileSize, size);
		tileSize *= blockFactor;
	}

	internal::le::analysis(size, frame);

	// Sequential relabel
	if (seqlabels != nullptr)
		internal::relabel(size, frame, seqlabels, &output.ccSlots);

	// Post-computation of properties
	if (output.hasProps() and options.computePropertiesLater) {
		CUTILS_SAFE( cudaDeviceSynchronize() );
		CUTILS_SAFE( cudaMemset(output.area    , 0, output.ccSlots * sizeof(    area_t)) );
		CUTILS_SAFE( cudaMemset(output.centroid, 0, output.ccSlots * sizeof(centroid_t)) );
		CUTILS_SAFE( cudaMemset(output.variance, 0, output.ccSlots * sizeof(variance_t)) );
		internal::transfer_props(size, output);
	}
}

} /* namespace lcg::cuccl */
} /* namespace lcg */
