#include <cuccl/basic_label.h>
#include <cuccl/ccl_data.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/le/analysis.h>
#include <cuccl/internal/find_root.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

__global__
void
analysisKernel(ccl_data data) {
	const cutils::vec2i P = image_indexing::globalPoint();
	const int   I = image_indexing::globalOffset(P);

	const basic_label label = data.labels[I];

	if (label.isForeground() and label.address() != I) {
		const basic_label root = findRoot<basic_label>(data.labels, label);

		if (root.address() != label.address())
			data.labels[I] = root;

		if (data.hasProps())
			data.unifyProps(basic_label(I), root);
	}
}

void analysis(const dim3 &size, ccl_data output) {
	const dim3 block(16, 8);
	const dim3 grid(size.x / block.x, size.y / block.y);

	analysisKernel<<<grid, block>>>(output);
}

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
