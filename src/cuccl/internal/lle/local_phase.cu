#include <cuccl/basic_label.h>
#include <cuccl/binary_segment.h>
#include <cuccl/ccl_data.h>
#include <cuccl/types.h>
//TODO: Fix texture-dependent functions in order to remove this dependency
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/le/textures.h>
#include <cuccl/internal/lle/local_phase.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace lle {

//TODO: Local properties could be represented in a more compact way in shared memory (less bytes), before being translated to global representations
template <class LabelType>
__device__ inline
void localAnalysis(label_t *labelsh, int i) {
	LabelType base = labelsh[i];

	if (base.isForeground()) {
		LabelType link = labelsh[base.address()];
		while (base.address() != link.address()) {
			base = link;
			link = labelsh[base.address()];
		}
	}

	__syncthreads();
	labelsh[i] = base;
}

template <class LabelType>
__device__ inline
void localScan(label_t *labelsh, bool &changed, const cutils::vec2i &p, int i) {
	LabelType label = labelsh[i];
	const label_t root = label.address();

	if (label.isForeground())
		neighbors_8::forEach(p, [&](const cutils::vec2i &dir, const cutils::vec2i &q) {
			if (image_indexing::inBlockRange(q)) {
				const int j = image_indexing::localOffset(q);
				const LabelType other = labelsh[j];
				if (label.connectedTo(other) and other < label) {
					label = label.link(other);
					changed = true;
				}
			}
		});

	__syncthreads();

	if (label.isForeground() and label.address() != root)
		atomicMin(labelsh + root, LabelType(labelsh[root]).link(label));
}

template <class LabelType>
__device__ inline
void localLabelEquivalence(label_t *labelsh, bool &changed, const cutils::vec2i &p, int i, int I) {
	// Perform local label equivalence iterations
	do {
		__syncthreads();
		changed = false;
		__syncthreads();
		localScan<LabelType>(labelsh, changed, p, i);
		__syncthreads();
		if (changed)
			localAnalysis<LabelType>(labelsh, i);
	} while (changed);

	LabelType label = labelsh[i];

	// Adjust local roots with corresponding global addresses
	if (label.address() == i)
		labelsh[i] = label = label.link(I);

	__syncthreads();

	// Adjust non-roots with global root address
	if (label.isForeground() and label.address() != I)
		labelsh[i] = label.link(labelsh[label.address()]);
}

//TODO: "Prelabel" should be removed from the name of this kernel. This is here only to make profiling code easier
__global__
void prelabelKernel(ccl_data data) {
	extern __shared__ label_t labelsh[];
	__shared__ bool changed;

	const cutils::vec2i P = image_indexing::globalPoint(); // Global 2D index
	const cutils::vec2i p = image_indexing:: localPoint(); // Local  2D index
	const int I = image_indexing::globalOffset(P); // Global 1D index
	const int i = image_indexing:: localOffset(p); // Local  1D index

	const binary_segment binaryValue = tex2D(le::binaryTex, P.x, P.y);

	if (binaryValue.isForeground()) {
		labelsh[i] = i;
	} else {
		labelsh[i] = basic_label::background();
	}

	localLabelEquivalence<basic_label>(labelsh, changed, p, i, I);

	data.labels[I] = labelsh[i];
	if (data.hasProps() and binaryValue.isForeground())
		data.addProps(basic_label(labelsh[i]), properties::of(P));
}

void localPhase(const dim3 &size, const segment_t *binary, ccl_data output, const dim3 &prefblock) {
	const dim3 block = (prefblock.x == 1 and prefblock.y == 1) ? dim3(16, 8) : prefblock;
	const dim3 grid(size.x / block.x, size.y / block.y);

	if (output.hasProps()) {
		CUTILS_FATAL( cudaMemset(output.area    , 0, size.x * size.y * sizeof(    area_t)) );
		CUTILS_FATAL( cudaMemset(output.centroid, 0, size.x * size.y * sizeof(centroid_t)) );
		CUTILS_FATAL( cudaMemset(output.variance, 0, size.x * size.y * sizeof(variance_t)) );
	}

	le::bindBinaryImageToTexture(binary, size);
	prelabelKernel<<<grid, block, sizeof(label_t) * block.x * block.y>>>(output);
}

} /* namespace lcg::cuccl::internal::lle */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */
