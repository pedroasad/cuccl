#ifndef LCG_CUCCL_DRIVER_OUTPUT_H_
#define LCG_CUCCL_DRIVER_OUTPUT_H_

namespace lcg {
namespace cuccl {

class ccl_data;

} /* namespace lcg::cuccl */
} /* namespace */

struct Terminal {
	static Terminal* getScreen(const dim3 &frameSize, const dim3 &outputSize, float fps, bool drawEllipses, bool wait);

	virtual ~Terminal();
	virtual void draw(lcg::cuccl::ccl_data &cpuccl_data) = 0;
	virtual bool quitRequested() const = 0;
};

#endif /* LCG_CUCCL_DRIVER_OUTPUT_H_ */
