#ifndef INPUT_H_
#define INPUT_H_

#include <boost/filesystem/path.hpp>
#include <cuccl/types.h>
#include <cutils/wrappers/hosted.h>

struct Source {
	static Source *openImage(const boost::filesystem::path &path, bool blackForeground, size_t repeat);
	static Source *openVideo(const boost::filesystem::path &path, bool blackForeground);

	virtual ~Source();
	virtual void close() = 0;
	virtual float fps() const = 0;
	virtual size_t frames() const = 0;
	virtual size_t framePixels() const = 0;
	virtual dim3 frameSize() const = 0;
	virtual bool read(lcg::cutils::hosted<lcg::cuccl::segment_image> &buffer) = 0;
};

#endif /* INPUT_H_ */
