#ifndef LCG_CUCCL_CORE_IMPROC_H_
#define LCG_CUCCL_CORE_IMPROC_H_

#include <cuccl/image.h>
#include <cutils/cuda_index.h>

namespace lcg {
namespace cuccl {

template<class Source, class Dest>
__global__
void copy(const Source *source, Dest *dest) {
	const int sindex = cutils::cuda_index::index1d();
	const int dindex = cutils::cuda_index::index1d();

	dest->data[dindex] = source->data[sindex];
}

template<class Source, class Dest, class RetardedCompilerDoesNotRecognizeSourceTypeForSecondParamater>
__global__
void swap_values(Source *source, Dest *dest, const typename Source::type a, const RetardedCompilerDoesNotRecognizeSourceTypeForSecondParamater b) {
	const int sindex = cutils::cuda_index::index1d();
	const int dindex = cutils::cuda_index::index1d();

	const typename Source::type value = source->data[sindex];

	if (value == a)
		dest->data[dindex] = b;
	else if (value == b)
		dest->data[dindex] = a;
}

template<class Source, class Dest>
__global__
void threshold(const Source *source, Dest *dest, const typename Source::type threshold) {
	const int sindex = cutils::cuda_index::index1d();
	const int dindex = cutils::cuda_index::index1d();

	if (source->data[sindex] > threshold)
		dest->data[dindex] = Dest::maxval;
	else
		dest->data[dindex] = Dest::minval;
}

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_CORE_IMPROC_H_ */
