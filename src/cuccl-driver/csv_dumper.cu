#include "csv_dumper.h"

#include <boost/filesystem.hpp>
#include <cuccl/ccl_data.h>
#include <cuccl/types.h>
#include <cutils/cuda_exception.h>

namespace fs = boost::filesystem;

CSVDumper::CSVDumper(const boost::filesystem::path &path) :
		outputStream(path.c_str()) {

	outputStream << "\"Frame\",\"CC size\"" << std::endl;
}

CSVDumper::~CSVDumper() {
	outputStream.close();
}

void CSVDumper::dump(size_t frame, lcg::cuccl::ccl_data &data) {
	CUTILS_SAFE( cudaDeviceSynchronize() );

	bool first = true;
	for (lcg::cuccl::label_t i = 0; i < data.ccSlots; i++) {
		lcg::cuccl::area_t area = data.area[i];
		if (area > 0) {
			if (first) {
				outputStream << area;
				first = false;
			} else {
				outputStream << "," << area;
			}
		}
	}

	outputStream << std::endl;
}
