#include "driver.h"
#include "parser.h"
#include <cuda_profiler_api.h>

int main(int argc, char *argv[]) {
	cudaProfilerStop();
	Driver::Options options = parseArgs(argc, argv);
	Driver *driver = new Driver(options);
	int exitcode = driver->run();
	delete driver;
	return exitcode;
}
