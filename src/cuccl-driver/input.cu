#include "input.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace lcg::cuccl;
using namespace lcg::cutils;
namespace fs = boost::filesystem;

struct ImageSource : Source {
	ImageSource(const fs::path &path, bool blackForeground, size_t repeat);
	virtual ~ImageSource();
	virtual void close();
	virtual float fps() const;
	virtual size_t frames() const;
	virtual size_t framePixels() const;
	virtual dim3 frameSize() const;
	virtual bool read(hosted<segment_image> &buffer);

private:
	cv::Mat _frame;
	size_t _repeat;
};

struct VideoSource : Source {
	VideoSource(const fs::path &path, bool blackForeground);
	virtual ~VideoSource();
	virtual void close();
	virtual float fps() const;
	virtual size_t frames() const;
	virtual size_t framePixels() const;
	virtual dim3 frameSize() const;
	virtual bool read(hosted<segment_image> &buffer);

private:
	cv::VideoCapture source;
	float _fps;
	size_t _frames;
	cv::Size _frameSize;
	bool _blackForeground;
};

Source* Source::openImage(const fs::path &path, bool blackForeground, size_t repeat) {
	return new ImageSource(path, blackForeground, repeat);
}

Source* Source::openVideo(const fs::path &path, bool blackForeground) {
	return new VideoSource(path, blackForeground);
}

Source::~Source() {}

ImageSource::ImageSource(const fs::path &path, bool blackForeground, size_t repeat) {
	cv::resize(cv::imread(path.native(), 0), _frame, cv::Size(LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT), 0, 0, cv::INTER_NEAREST);
//	_frame = cv::imread(path.native(), 0);
	_repeat = repeat;

	if (not blackForeground)
		_frame = ~_frame;

	cv::threshold(_frame, _frame, 128, 255, CV_THRESH_BINARY);
}

ImageSource::~ImageSource() {
}

void ImageSource::close() {
	_repeat = 0;
}

float ImageSource::fps() const {
	return 30.0f;
}

size_t ImageSource::frames() const {
	return _repeat;
}

size_t ImageSource::framePixels() const {
	return _frame.total();
}

dim3 ImageSource::frameSize() const {
	return dim3(_frame.size[1], _frame.size[0], 1);
}

bool ImageSource::read(hosted<segment_image> &buffer) {
	cv::Mat finalImage(_frame.size[0], _frame.size[1], CV_8UC1, (uint8_t*) buffer->data);

	if (_repeat-- > 0) {
		_frame.copyTo(finalImage);
		return true;
	} else {
		return false;
	}
}

VideoSource::VideoSource(const fs::path &path, bool blackForeground) {
	source.open(path.native());
	_fps = source.get(CV_CAP_PROP_FPS);
	_frames = source.get(CV_CAP_PROP_FRAME_COUNT);
	_frameSize = cv::Size(LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT);
//	_frameSize = cv::Size(
//		source.get(CV_CAP_PROP_FRAME_WIDTH ),
//		source.get(CV_CAP_PROP_FRAME_HEIGHT)
//	);
	_blackForeground = blackForeground;
}

VideoSource::~VideoSource() {
	source.release();
}

void VideoSource::close() {
	source.release();
}

float VideoSource::fps() const {
	if (source.isOpened()) {
		return std::isnan(_fps) ? 30.0f : _fps;
	} else {
		throw std::logic_error("Cannot determine FPS of a closed video");
	}
}

size_t VideoSource::frames() const {
	if (source.isOpened()) {
		return _frames;
	} else {
		throw std::logic_error("Cannot determine frame count of a closed video");
	}
}

size_t VideoSource::framePixels() const {
	return _frameSize.width * _frameSize.height;
}

dim3 VideoSource::frameSize() const {
	if (source.isOpened()) {
		return dim3(_frameSize.width, _frameSize.height);
	} else {
		throw std::logic_error("Cannot determine frame size of a closed video");
	}
}

bool VideoSource::read(hosted<segment_image> &buffer) {
	cv::Mat frame;
	cv::Mat finalImage(_frameSize, CV_8UC1, buffer->data);
	bool result = source.isOpened() and source.read(frame);

	if (result) {
		cv::resize   (frame, frame, _frameSize, 0, 0, cv::INTER_NEAREST);
		cv::cvtColor (frame, frame, CV_BGR2GRAY);
		if (not _blackForeground)
			frame = ~frame;
		cv::threshold(frame, finalImage, 128, 255, CV_THRESH_BINARY);
	}

	return result;
}
