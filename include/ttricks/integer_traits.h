#ifndef LCG_TTRICKS_INTEGER_TRAITS_H_
#define LCG_TTRICKS_INTEGER_TRAITS_H_

#include <climits>
#include <limits>

namespace lcg {
namespace ttricks {

template <class Type>
struct integer_traits : std::numeric_limits<Type> 
{};

template <class Type, Type min_val, Type max_val>
struct integer_traits_base {
	static const Type minval = min_val;
	static const Type maxval = max_val;
};

template <>
struct integer_traits<char> : 
	public std::numeric_limits<char>, 
	public integer_traits_base<char, CHAR_MIN, CHAR_MAX> 
{};

template <>
struct integer_traits<unsigned char> :
	public std::numeric_limits<unsigned char>,
	public integer_traits_base<unsigned char, 0, UCHAR_MAX> 
{};

template <>
struct integer_traits<short> :
	public std::numeric_limits<short>,
	public integer_traits_base<short, SHRT_MIN, SHRT_MAX> 
{};

template <>
struct integer_traits<unsigned short> :
	public std::numeric_limits<unsigned short>,
	public integer_traits_base<unsigned short, 0, USHRT_MAX> 
{};

template <>
struct integer_traits<int> :
	public std::numeric_limits<int>,
	public integer_traits_base<int, INT_MIN, INT_MAX> 
{};

template <>
struct integer_traits<unsigned int> :
	public std::numeric_limits<unsigned int>,
	public integer_traits_base<unsigned int, 0, UINT_MAX> 
{};

template <>
struct integer_traits<long int> :
	public std::numeric_limits<long int>,
	public integer_traits_base<long int, LONG_MIN, LONG_MAX> 
{};

template <>
struct integer_traits<unsigned long int> :
	public std::numeric_limits<unsigned long int>,
	public integer_traits_base<unsigned long int, 0, ULONG_MAX> 
{};

template <>
struct integer_traits<long long int> :
	public std::numeric_limits<long long int>,
	public integer_traits_base<long long int, LLONG_MIN, LLONG_MAX> 
{};

template <>
struct integer_traits<unsigned long long int> :
	public std::numeric_limits<unsigned long long int>,
	public integer_traits_base<unsigned long long int, 0, ULLONG_MAX> 
{};

} /* namespace ttricks */
} /* namespace lcg */

#endif /* LCG_TTRICKS_INTEGER_TRAITS_H_ */
