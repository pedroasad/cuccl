#ifndef LCG_CUCCL_BASIC_LABEL_H_
#define LCG_CUCCL_BASIC_LABEL_H_

#include <cstdint>
#include <cuccl/types.h>
#include <cutils/wrappers/type_wrapper.h>

namespace lcg {
namespace cuccl {

/**
 * Wrapper class for \c label_t conforming with the <em>label wrapping concept
 * </em> and used in many CCL algorithms.
 *
 * \see The \ref concepts section for more information on the <em>label wrapping
 * concept</em>.
 */
class basic_label : public cutils::type_wrapper<label_t> {
public:
	using cutils::type_wrapper<label_t>::type_wrapper;

	__HOST__DEVICE__IF_CUDA inline
	static constexpr label_t background() {
		return std::numeric_limits<label_t>::max();
	}

	__HOST__DEVICE__IF_CUDA inline
	label_t address() const {
		return (*this);
	}

	__HOST__DEVICE__IF_CUDA inline
	bool connectedTo(label_t) const {
		return true;
	}

	__HOST__DEVICE__IF_CUDA inline
	label_t flags() const {
		return 0;
	}

	__HOST__DEVICE__IF_CUDA inline
	label_t link(label_t value) const {
		return value;
	}

	__HOST__DEVICE__IF_CUDA inline
	bool isBackground() const {
		return (*this) == basic_label::background();
	}

	__HOST__DEVICE__IF_CUDA inline
	bool isForeground() const {
		return not isBackground();
	}
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_BASIC_LABEL_H_ */
