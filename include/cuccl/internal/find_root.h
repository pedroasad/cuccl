#ifndef LCG_CUCCL_INTERNAL_FIND_ROOT_H_
#define LCG_CUCCL_INTERNAL_FIND_ROOT_H_

#include <cuccl/types.h>

namespace lcg {
namespace cuccl {
namespace internal {

template <class LabelType>
__device__ inline
label_t findRoot(const label_t *table, label_t label) {

	LabelType base, link;

	base = label;
	link = table[base.address()];

	while (base.address() != link.address()) {
		base = link;
		link = table[base.address()];
	}

	return base;
}

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_FIND_ROOT_H_ */
