#ifndef LCG_CUCCL_INTERNAL_LE_SCAN_H_
#define LCG_CUCCL_INTERNAL_LE_SCAN_H_

#include <cuccl/basic_label.h>
#include <cuccl/types.h>
#include <cuccl/internal/image_indexing.h>
#include <cuccl/internal/smallest.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {
namespace le {

template <class NeighborSelector>
__global__
void scanKernel(bool *changed, label_t *labels, NeighborSelector find, bool useLocalFlag) {
	__shared__ bool localChanged;

	const cutils::vec2i P = image_indexing::globalPoint();
	const int   I = image_indexing::globalOffset(P);
	const int   i = image_indexing::localOffset();

	if (useLocalFlag and changed != nullptr) {
		localChanged = false;
		__syncthreads();
	}

	const basic_label label = tex2D(labelTex, P.x, P.y);

	if (label.isForeground()) {
		const label_t minimal = find(P);

		if (minimal < label) {
			if (changed != nullptr) {
				if (useLocalFlag)
					localChanged = true;
				else
					*changed = true;
			}
			atomicMin(labels + label.address(), minimal);
		}
	}

	if (useLocalFlag and changed != nullptr) {
		__syncthreads();
		if (i == 0 and localChanged)
			*changed = true;
	}
}

template <class NeighborSelector>
inline void
scan(const dim3 &size, label_t *labels, bool *changed, NeighborSelector find, bool useLocalFlag=true) {
	const dim3 block(16, 8);
	const dim3 grid(size.x / block.x, size.y / block.y);

	bindLabelImageToTexture(labels, size);
	scanKernel<<<grid, block>>>(changed, labels, find, useLocalFlag);
}

} /* namespace lcg::cuccl::internal::le */
} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_LE_SCAN_H_ */
