#ifndef LCG_CUCCL_INTERNAL_ANY_SMALLER_H_
#define LCG_CUCCL_INTERNAL_ANY_SMALLER_H_

#include <cuccl/types.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {

template <class Neighbors>
struct any_smaller_neighbor {
	//TODO: Parameterize by texture object and remove dependency on le_textures.h
	//TODO: Create a concept for this kind of object
	__device__ inline
	label_t operator()(const cutils::vec2i &p) {
		using cutils::vec2i;

		label_t smallest = tex2D(le::labelTex, p.x, p.y);

		Neighbors::forEachCheck(p, [&](const vec2i &dir, const vec2i &q) -> bool {
			const label_t label = tex2D(le::labelTex, q.x, q.y);

			if (label < smallest) {
				smallest = label;
				return false;
			} else {
				return true;
			}
		});

		return smallest;
	}
};

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_INTERNAL_ANY_SMALLER_H_ */
