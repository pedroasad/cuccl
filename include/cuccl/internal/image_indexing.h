#ifndef LCG_CUCCL_IMAGE_INDEXING_H_
#define LCG_CUCCL_IMAGE_INDEXING_H_

#include <cuccl/types.h>
#include <cuccl/internal/le/textures.h>

namespace lcg {
namespace cuccl {
namespace internal {

struct image_indexing {
	__device__ inline
	static int globalOffset(const cutils::vec2i &point) {
		return point.dot(cutils::vec2i(1, blockDim.x * gridDim.x));
	}

	__device__ inline
	static cutils::vec2i globalPoint() {
		return {
			blockIdx.x * blockDim.x + threadIdx.x,
			blockIdx.y * blockDim.y + threadIdx.y
		};
	}

	__device__ inline
	static int globalOffset() {
		return globalOffset(globalPoint());
	}

	__device__ inline
	static cutils::vec2i globalRange() {
		return {
			blockDim.x * gridDim.x,
			blockDim.y * gridDim.y
		};
	}

	__device__ inline
	static bool inBlockRange(const cutils::vec2i &point) {
		return point.x >= 0 and point.y >= 0 and point.x < blockDim.x and point.y < blockDim.y;
	}

	__device__ inline
	static bool inGridRange(const cutils::vec2i &point) {
		return point.x >= 0 and point.y >= 0 and point.x < blockDim.x * gridDim.x and point.y < blockDim.y * gridDim.y;
	}

	__device__ inline
	static int localOffset(const cutils::vec2i &point) {
		return point.dot(cutils::vec2i(1, blockDim.x));
	}

	__device__ inline
	static int localOffset() {
		return localOffset(localPoint());
	}

	__device__ inline
	static cutils::vec2i localPoint() {
		return {
			threadIdx.x,
			threadIdx.y
		};
	}

	__device__ inline
	static cutils::vec2i localRange() {
		return {
			blockDim.x,
			blockDim.y
		};
	}

	__device__ inline
	static unsigned totalHeight() {
		return blockDim.y * gridDim.y;
	}

	__device__ inline
	static cutils::vec2i totalSize() {
		return globalRange();
	}

	__device__ inline
	static unsigned totalWidth() {
		return blockDim.x * gridDim.x;
	}

	__device__ inline
	static unsigned blocksPerGrid() {
		return gridDim.x * gridDim.y;
	}

	__device__ inline
	static unsigned threadsPerBlock() {
		return blockDim.x * blockDim.y;
	}

	__device__ inline
	static unsigned threadsPerGrid() {
		return blocksPerGrid() * threadsPerBlock();
	}
};

} /* namespace lcg::cuccl::internal */
} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_IMAGE_INDEXING_H_ */
