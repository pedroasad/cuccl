#ifndef LCG_CUCCL_GTM_H_
#define LCG_CUCCL_GTM_H_

#include <cuccl/ccl_algorithm.h>
#include <cuccl/types.h>

namespace lcg {
namespace cuccl {

class ccl_data;

class TileMerging : public ccl_algorithm {
public:
	struct Options {
		bool computePropertiesLater;
		bool sequentialRelabel;

		int tileSize;
		int blockFactor;

		Options();
	};

	TileMerging(const dim3 &size, Options options);

	virtual
	~TileMerging();

	virtual void
	ccl(const segment_t *binary, ccl_data &output);

private:
	Options options;
	int maxThreadsPerBlock;
	dim3 size;
	label_t *seqlabels;
	cutils::pinned<bool> changed;
};

} /* namespace lcg::cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_GTM_H_ */
