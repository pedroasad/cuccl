/** @file
 * Basic data-types used in the CuCCL library.
 */
#ifndef LCG_CUCCL_TYPES_H_
#define LCG_CUCCL_TYPES_H_

#include <cstdint>
#include <iterator>
#include <cuccl/image.h>
#include <cutils/cuda_index.h>
#include <cutils/macros.h>
#include <cutils/vector.h>
#include <cutils/wrappers.h>

//TODO: Review the role of these constants. Should they be indeed upper limits?
#ifndef LCG_CUCCL_MAX_WIDTH
#define LCG_CUCCL_MAX_WIDTH 12 * 320
#endif

#ifndef LCG_CUCCL_MAX_HEIGHT
#define LCG_CUCCL_MAX_HEIGHT 12 * 180
#endif

namespace lcg {
namespace cuccl {

////////////////////////////////////////////////////////////////////////////////
/// Basic typedefs
////////////////////////////////////////////////////////////////////////////////

typedef uint8_t segment_t; //!< Type used for pixel segments (foreground vs. background, for instance).
typedef uint32_t  label_t; //!< Type used for pixel labels.

typedef unsigned                                  area_t; //!< Type used for region area (number of pixels).
typedef cutils::vector<unsigned long long, 2> centroid_t; //!< Type used for region centroid.
typedef cutils::vector<unsigned long long, 3> variance_t; //!< Type used for region variance.

//TODO: Use size-agnostic buffers instead of these predefined arrays. Maybe replace with thrust::vectors
typedef image<segment_t, LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT> segment_image;
typedef image<  label_t, LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT>   label_image;

typedef array_2d<    area_t, LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT>     area_table;
typedef array_2d<centroid_t, LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT> centroid_table;
typedef array_2d<variance_t, LCG_CUCCL_MAX_WIDTH, LCG_CUCCL_MAX_HEIGHT> variance_table;

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_TYPES_H_ */
