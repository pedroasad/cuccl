#ifndef LCG_CUTILS_TYPE_WRAPPER_H_
#define LCG_CUTILS_TYPE_WRAPPER_H_

#include <cutils/macros.h>

namespace lcg {
namespace cutils {

/**
 * A template base class for wrapping primitive types. Any instantiation of
 * this template with a primitive type \c T yields a type that can be treated
 * as if it were an extension of \c T, that is
 * * It may be used in logic/arithmetic operations
 * * It may be assigned and initialized with expressions convertible to \c T
 * * It is automatically converted to \c T whenever needed.
 * This aims at facilitating implementation of Java-style wrapper classes. By
 * extending any instantiation of this class, it is easy to "add methods to
 * primitive types".
 */
template <class WrappedType>
class type_wrapper {
public:
	__HOST__DEVICE__IF_CUDA inline
	type_wrapper(WrappedType value = WrappedType()) : _value(value) {
	}

	__HOST__DEVICE__IF_CUDA inline
	operator WrappedType() const {
		return _value;
	}

	__HOST__DEVICE__IF_CUDA inline
	operator WrappedType&() {
		return _value;
	}

private:
	WrappedType _value;
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_TYPE_WRAPPER_H_ */
