#ifndef LCG_CUTILS_WRAPPERS_HOST_H
#define LCG_CUTILS_WRAPPERS_HOST_H

#include <stdexcept>
#include <cutils/cuda_exception.h>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template<class Type>
class hosted {
	Type instance;

public:
	hosted() :
			instance() {
	}

	hosted(const Type &data) :
			instance(data) {
	}

	hosted(const hosted &other) :
			instance(other) {
	}

	hosted(const hosted_ref<Type> &other) :
			instance(other) {
	}

	hosted(const pinned<Type> &other) :
			instance(other) {
	}

	hosted(const device<Type> &other) :
			instance() {
		CUTILS_SAFE( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted(const device_ref<Type> &other) :
			instance() {
		CUTILS_SAFE( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted& operator= (const Type &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const hosted &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const hosted_ref<Type> &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const pinned<Type> &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const device<Type> &other) {
		CUTILS_SAFE( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	hosted& operator= (const device_ref<Type> &other) {
		CUTILS_SAFE( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	operator Type&() {
		return instance;
	}

	operator const Type&() const {
		return instance;
	}

	Type& operator*() {
		return instance;
	}

	const Type& operator*() const {
		return instance;
	}

	Type* operator->() {
		return &instance;
	}

	const Type* operator->() const {
		return &instance;
	}

	Type* operator&() {
		return &instance;
	}

	const Type* operator&() const {
		return &instance;
	}
};

template <class Type>
class hosted<Type*> {
	friend class device<Type*>;
	friend class device_ref<Type*>;

	size_t length;
	Type *array;
public:
	hosted(size_t length) :
			length(length), array(new Type[length]) {
	}

	template <size_t N>
	hosted(const Type (&data)[N]) :
			length(N), array(new Type[N]) {
		memcpy(array, data, length * sizeof(Type));
	}

	//TODO: Missing constructor
//	hosted(const pinned<Type*> &other) :
//			length(other.length), array(new Type[other.length]) {
//		memcpy(array, other, length * sizeof(Type));
//	}

	hosted(const device<Type*> &other) :
			length(other.length), array(new Type[other.length]) {
		CUTILS_SAFE( cudaMemcpy(array, other, length * sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted(const device_ref<Type*> &other) :
			length(other.length), array(new Type[other.length]) {
		CUTILS_SAFE( cudaMemcpy(array, other, length * sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	operator Type*() {
		return array;
	}

	operator const Type*() const {
		return array;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_WRAPPERS_HOST_H */
