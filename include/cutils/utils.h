#ifndef LCG_CUTILS_UTILS_H_
#define LCG_CUTILS_UTILS_H_

namespace lcg {
namespace cutils {

//TODO: Use __HOST__DEVICE__IF_CUDA wherever possible
template <class Type, int N>
__host__ __device__
size_t length(const Type (&array)[N]) {
	return N;
}

template <typename T>
__host__ __device__
int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

template <class Type>
__host__ __device__
Type sqr(const Type &k) {
	return k * k;
}

template <class Type>
__host__ __device__
void swap(Type &a, Type &b) {
	Type c = a;
	a = b;
	b = c;
}

template <class Numeric>
Numeric sum(const Numeric &n, const Numeric &x = 0) {
	return x * n + n * (n - 1) / 2;
}

template <class Numeric>
Numeric sum_sqr(const Numeric &n, const Numeric &x = 0) {
	return x * x * n + x * n * (n-1) + (2 * n * n * n + 3 * n * n + n) / 6;
}

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_UTILS_H_ */
